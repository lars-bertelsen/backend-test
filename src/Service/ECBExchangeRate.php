<?php


namespace Opeepl\BackendTest\Service;


class ECBExchangeRate
{

	public function __construct(){

	}

	/**
	 * Return all supported currencies
	 *
	 * @return array<string>
	 */
	public function getSupportedCurrencies(): array {
		return array("CAD" ,"HKD" ,"ISK" ,"PHP" ,"DKK" ,"HUF" ,"CZK" ,"AUD" ,"RON" ,"SEK" ,"IDR" ,"INR" ,"BRL" ,"RUB" ,"HRK" ,"JPY" ,"THB" ,"CHF" ,"SGD" ,"PLN" ,"BGN" ,"TRY" ,"CNY" ,"NOK" ,"NZD" ,"ZAR" ,"USD" ,"MXN" ,"ILS" ,"GBP" ,"KRW" ,"MYR", "EUR");
	}

	/**
	 *
	 * Get the exchangerate between from and to currency
	 *
	 * @param string $fromCurrency
	 * @param string $toCurrency
	 * @return float
	 */
	public function getExchangeRate(string $fromCurrency, string $toCurrency): float {
		if($fromCurrency == $toCurrency){
			return 1;
		}

		$url = "https://api.exchangeratesapi.io/latest?base=".$fromCurrency;
		$output = file_get_contents($url);

		$json = json_decode($output, true);

		return $json["rates"][$toCurrency];
	}
}